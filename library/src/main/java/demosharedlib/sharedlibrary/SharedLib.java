package demosharedlib.sharedlibrary;

public class SharedLib {
    String message;

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void printMessage() {
        System.out.println("Demo shared library: " + this.message);
    }
}
